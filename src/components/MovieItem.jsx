import PropTypes from "prop-types";
import React, {useState, useEffect} from "react";
import "./MovieItem.css";

function MovieItem(props) {
  const { title, released, director, poster } = props; //Décomposition des props

  // Utilisation de useState pour gérer l'état du favori (film favori ou non favori)
  const [isFavorite, setIsFavorite] = useState(() => {
    // Récupération de la valeur stockée dans le localStorage, ou false à défaut
    return JSON.parse(localStorage.getItem("favoriteMovies")) || false;
  });

  // Cette fonction est appelée lorsqu'on clique sur le bouton. Elle utilise setIsFavorite pour inverser l'état actuel du film entre favori et non favori.
  const toggleFavorite = () => {
    setIsFavorite(!isFavorite);
  };

  // Utilisation de useEffect pour sauvegarder l'état dans le localStorage
  useEffect(() => {
    localStorage.setItem("favoriteMovies", JSON.stringify(isFavorite));
  }, [isFavorite]); // Écoute des changements de l'état isFavorite.L'effet ne doit être exécuté que lorsque isFavorite change.

  return (
    <div className="MovieItem">
      <h2>{title}</h2>
      <img src={poster} alt={title} />
      <h4>Director: {director}</h4>
      <h5>Released: {released}</h5>
      <button onClick={toggleFavorite}>
        {isFavorite ? "Retirer des favoris" : "Ajouter aux favoris"}
      </button>
    </div>
  );
}


MovieItem.propTypes = {
  title: PropTypes.string.isRequired,
  released: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
};

export default MovieItem;
